#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
struct _dictionary
	{
		int code;
		char newString[8];
	};
	typedef struct _dictionary dictionary;
	dictionary codeDictionary[256];
int main()
{
	char* stringToCompress;
	char strNow[7] = "", lastChar[2] = "", currChar[2] = "", nextChar = "";
	int outputCode = 0, currentPosition = 0, sizeOfDict, outputCodes[256], outputPos = 0;
	bool inDictionary = false;
	
	stringToCompress = (char*)malloc(sizeof(char) * 129);

	codeDictionary[0].code = 0;
	codeDictionary[0].newString[0] = '#';
	nextChar = 'A';


	for (int i = 1; i < 27; i++)
	{
		codeDictionary[i].code = i;
		codeDictionary[i].newString[0] = nextChar;
		codeDictionary[i].newString[1] = NULL;
		nextChar += 1;
	};


	codeDictionary[27].code = 27;
	codeDictionary[27].newString[0] = ' ';
	codeDictionary[28].code = 28;
	codeDictionary[28].newString[0] = '.';
	sizeOfDict = 28;


	int checkDictionary(char* string, dictionary* dictArray, bool *inDictionary, int sizeOfDict, int *output);
	int addToDictionary(char* string, dictionary* dictArray, int* sizeOfDict);


	strcpy(stringToCompress, "THERE ARE ONLY TWO WAYS TO LIVE YOUR LIFE. ONE IS AS THOUGH NOTHING IS A MIRACLE. THE OTHER IS AS THOUGH EVERYTHING IS A MIRACLE.");
	
	do
	{
		int i = 0;
		strcpy(strNow, "");
			strcpy(lastChar, currChar);
			currChar[0] = stringToCompress[currentPosition + i];
			currChar[1] = NULL;
			do
			{
				strcat(strNow, currChar);


				checkDictionary(strNow, codeDictionary, &inDictionary, sizeOfDict, &outputCode);
				i += 1;


				if (inDictionary == true)
				{
					strcpy(lastChar, currChar);
					currChar[0] = stringToCompress[currentPosition + i];
					currChar[1] = NULL;
				}
				if (i > 2)
				{
					strcpy(lastChar, currChar);
				}

			} while ((inDictionary == true) && (currChar != "\0"));

			if (inDictionary == false)
			{

				addToDictionary(strNow, codeDictionary, &sizeOfDict);
				outputCodes[outputPos] = outputCode;
				outputPos += 1;
			}

			currentPosition += (i-1);
		
	} while (currentPosition <= (strlen(stringToCompress)-2));
	/*for (int i = 0; i < (outputPos); i++)
	{
		printf("%d\n", outputCodes[i]);
	}*/
	FILE* fp = fopen("compressed.dat", "wb");
	fwrite(outputCodes, sizeof(int), outputPos, fp);
	fclose(fp);
	free(stringToCompress);
	return 0;
}


int checkDictionary(char* string, dictionary* dictArray, bool *inDictionary, int sizeOfDict, int *output)
{
	*inDictionary = false;
	for (int i = 0; i < (sizeOfDict + 1); i++)
	{
		if (!(strcmp(string, dictArray[i].newString)))
		{
			*inDictionary = true;
			*output = dictArray[i].code;
			break;
		}
	}
	return 0;
}


int addToDictionary(char* string, dictionary*  dictArray, int* sizeOfDict)
{
	*sizeOfDict += 1;
	strcpy(dictArray[*sizeOfDict].newString, string);
	dictArray[*sizeOfDict].code = *sizeOfDict;
	return 0;
}